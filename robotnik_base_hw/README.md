# robotnik_base_hw

ROS controller component based on ros_control architecture. 

This component is compatible with most of the Robotnik's motor hardware.

## Dependencies

This component depends on the library robotnik_base_hw_lib

## Installation

* Run "dpkg -i robotnik_base_hw_lib.deb". This copy the library librobotnik_base_hw.so to /usr/lib and copy the headers into /usr/include/robotnik_base_hw_lib


