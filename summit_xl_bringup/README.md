# summit_xl_robot
includes all the packages required for the robot control.

## summit_xl_bringup
This package contains the launch files needed to run the SUMMIT XL robot.

We have to create a file named **summitxl_params_ID.env**. Besides, we have to include `source ~/summitxl_params_IdPedido.env` to the *.bashrc* file. This file contains environment variables that establish the actual components of the robot. For example: 
```
export ROBOT_ID=summit_xl
export ROBOT_XACRO=versions/summit_xl_**ID**.urdf.xacro
export ROBOT_LASER_MODEL=ug01_laser
export ROBOT_HAS_LASER=false
export ROBOT_HAS_AXIS_CAMERA=false
export ROBOT_HAS_GPS=false
export ROBOT_HAS_RGBD_CAMERA=false
export ROBOT_PAD_MODEL=ps4
export ROBOT_GEARBOX=12.52
export ROBOT_HAS_ENCODER=false
```
##### Explanation of each environment variable
- `ROBOT_ID` indicates the name of the robot. This is the name of the namespace under all the nodes will be working. This is also used as the prefix of all the subcomponents.(*summit_xl*)
- `ROBOT_XACRO` indicates the path where the xacro file is. (inside the robot folder in robot_description)(*summit_xl.urdf.xacro*)
- `ROBOT_LASER_MODEL` indicates the model of the laser that the robot is using. The model is the name of the launch file.(*sick_tim561/hokuyo_ug01/hokuyo_ust*)
- `ROBOT_HAS_LASER` indicates if the robot has a laser. (*true/false*)
- `ROBOT_HAS_AXIS_CAMERA` indicates if the robot has the axis camera.
- `ROBOT_HAS_GPS` indicates if the robot has gps. (*true/false*)
- `ROBOT_HAS_RGBD_CAMERA` indicates if the robot has a rgbd camera. (*true/false*)
- `ROBOT_PAD_MODEL` pad model used. (*ps4/ps3/logitechf710/xbox360*)
- `ROBOT_GEARBOX` establishes the motor gearbox value. (*24V: 12.52 | 48V: 9.56*)
- `ROBOT_HAS_ENCODER` indicates if the robot has encoders. (*true/false*)
