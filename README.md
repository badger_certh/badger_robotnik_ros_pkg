# badger_robotnik_ros_pkg

## summit_xl_robot
ROS package of the Summit XL, XL-HL mobile robot platforms

![Image of Summit XL](http://www.robotnik.es/web/wp-content/uploads/2014/03/summit-xl-robots-moviles-robotnik_s01.jpg)

<h2>summit_xl_bringup</h2>

This package contains launch files to start the robot. The most relevant file is launch/summit_xl_complete.launch, that is called from the .bashrc at system startup. The file can be configured to start the available devices, usually: lasers, cameras, rgbd devices, imus, gps, etc. Integrates several test launch files to test the components individually. This file defines the robot serial number as parameter.

<h2>summit_xl_controller</h2>

This package defines the summit_xl_controller/SummitXLController as controller_interface::ControllerBase class of ROS Control. The Summit XL Controller controls 4 wheel mobile platforms both in skid and omnidirectional drive. It expects 4 VelocityJointInterface type of hardware interfaces. It contains the robot control functions to operate the skid-steering / omni-steering structure (velocity control of the axes and position control of the robot) and to get accurate odometry estimations from the robot sensors. This node subscribes to cmd_vel messages. Alternatively the robot can also be controlled using the ROS diff_drive_controller, but this controller has less features, e.g. it does not allow the control as omnidirectional platform.

<h2>summit_xl_hw</h2>

Package to manage the Summit XL / Summit XL HL / Summit XL Steel - servo controller set. Hardware interface to the 4 servoamplifiers compliant with ROS Control and associated digital I/O. This node publishes the battery voltage and permits the definition of an alarm voltage.

<h2>summit_xl_robot</h2>

summit_xl_robot metapackage folder.

<h2>summit_xl_web</h2>

rosbridge server based package to access the robot via web browser. It allows to access the status of the robot, the diagnostics of the servos, the camera, battery level, errors, etc. It allows to control the platform motion, but also the PTZ. The package is intended as a template to be further expanded by the user.


## summit_xl_common
Common packages of the Summit XL: URDF description of the Summit XL and Summit XL HL, platform messages and other files for simulation.

![Image of Summit XL](http://www.robotnik.es/web/wp-content/uploads/2014/03/summit-xl-robots-moviles-robotnik_s01.jpg)

<h2>summit_xl_description</h2>

The urdf, meshes, and other elements needed in the description are contained here. The standard camera configurations have been included (w/wo sphere_camera, w/wo axis_camera, etc.). This package includes the description of the Summit XL, Summit XL OMNI (and HL versions) mobile platforms.
The package includes also some launch files to publish the robot state and to test the urdf files in rviz.

<h2>summit_xl_localization</h2>

This package contains launch files to use the EKF of the robot_localization package with the Summit XL robots. It contains a node to subscribe to gps data and publish it as odometry to be used as an additional source for odometry.

<h2>summit_xl_navigation</h2>

This package contains all the configuration files needed to execute the AMCL and SLAM navigation algorithms in simulation.

<h2>summit_xl_pad</h2>

This package contains the node that subscribes to /joy messages and publishes command messages for the robot platform including speed level control. The joystick output is feed to a mux (http://wiki.ros.org/twist_mux) so that the final command to the robot can be set by different components (move_base, etc.)

The node allows to load different types of joysticks (PS4, PS3, Logitech, Thrustmaster). New models can be easily added by creating new .yaml files. If modbus_io node is available, the digital outputs (ligths, axes, etc.) can also be controlled with the pad. If ptz camera is available, the pan-tilt-zoom can also be commanded with the pad.




